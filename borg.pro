TEMPLATE = app
TARGET = backup
INCLUDEPATH += .

# Input
HEADERS += AbstractCommand.h \
           Backup.h \
           CommandLineParser.h \
           BorgRunner.h \
           Logger.h \
           Logger_p.h \
           commands/Add.h \
           commands/List.h \
           commands/Prune.h \
           commands/Revert.h \
           commands/Sync.h \

SOURCES += AbstractCommand.cpp \
           Backup.cpp \
           CommandLineParser.cpp \
           BorgRunner.cpp \
           Logger.cpp \
           commands/Add.cpp \
           commands/List.cpp \
           commands/Prune.cpp \
           commands/Revert.cpp \
           commands/Sync.cpp \

