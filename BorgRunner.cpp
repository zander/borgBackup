/*
 * This file is part of the borg-backup project
 * Copyright (C) 2008-2021 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "BorgRunner.h"
#include "Logger.h"

BorgRunner::BorgRunner(QProcess &process, const QStringList &arguments)
    : m_process(&process),
    m_arguments(arguments),
    m_timeout(-1)
{
}

int BorgRunner::start(WaitCondition condition, Fail fail)
{
    Q_ASSERT(condition <= WaitUntilReadyForWrite);
    if (Logger::verbosity() >= Logger::Debug) {
        Logger::debug() << "command:  borg";
        foreach (QString arg, m_arguments) {
            Logger::debug() << " '" << arg << "'";
        }
        Logger::debug() << Qt::endl;
        Logger::debug().flush();
    }

    if (condition == WaitForStandardOutput)
        m_process->setReadChannel(QProcess::StandardOutput);
    else if (condition == WaitForStandardError) {
        m_process->setReadChannel(QProcess::StandardError);
        condition = WaitForStandardOutput;
    }

    QIODevice::OpenMode mode = QIODevice::ReadOnly;
    if (condition == WaitUntilReadyForWrite)
        mode |= QIODevice::WriteOnly;

    m_process->start(QLatin1String("borg"), m_arguments, mode);
    if ((condition == WaitForStandardOutput && !m_process->waitForReadyRead(m_timeout))
        || (condition == WaitUntilFinished && !m_process->waitForFinished(m_timeout))
        || (condition == WaitUntilReadyForWrite && !m_process->waitForStarted(m_timeout))) {
        if (m_process->state() != QProcess::NotRunning) { // time out
            Logger::error() << "ERROR: the backend (borg) fails to start, timing out" << Qt::endl;
            m_process->kill();
            return 1;
        }
        if (m_process->exitStatus() == QProcess::NormalExit) {
            if (m_process->exitCode() != 0) {
                if (fail == WarnOnFail)
                    Logger::error() << "ERROR: Failed calling borg" << Qt::endl; // we may have to change this to debug later.
                Logger::debug() << "   we got return code: " << m_process->exitCode() << Qt::endl;
                return 1;
            }
            return 0;
        }
        Logger::error() << "ERROR: the backend (borg) crashed" << Qt::endl;
        return 1;
    }
    if (m_process->exitCode() != 0) {
        Logger::debug() << "   we got return code: " << m_process->exitCode() << Qt::endl;
        return 1;
    }
    return 0;
}

void BorgRunner::setArguments(const QStringList &arguments)
{
    m_arguments = arguments;
}
