/*
 * This file is part of the borg-backup project
 * Copyright (C) 2008-2021 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "AbstractCommand.h"
#include "CommandLineParser.h"
#include "Logger.h"

#include <QDebug>
#include <QSettings>

static const CommandLineOption options[] = {
    {"-v, --verbose", "give verbose output"},
    {"-q, --quiet", "suppress informational output"},
    {"--debug", "print debugging information"},
    {"--standard-verbosity", "neither verbose nor quiet output"},
    {"-h, --help", "shows brief description of command and its arguments"},
    CommandLineLastOption
};

AbstractCommand::AbstractCommand(const char *name)
    : m_name(QString::fromLatin1(name))
{
    CommandLineParser::addOptionDefinitions(options);
}

AbstractCommand::~AbstractCommand()
{
}

int AbstractCommand::start()
{
    CommandLineParser *args = CommandLineParser::instance();

    if (args->contains(QLatin1String("quiet")))
        Logger::setVerbosity(Logger::Quiet);
    else if (args->contains(QLatin1String("verbose")))
        Logger::setVerbosity(Logger::Verbose);
    else if (args->contains(QLatin1String("standard-verbosity")))
        Logger::setVerbosity(Logger::Verbose);
    else if (args->contains(QLatin1String("debug")))
        Logger::setVerbosity(Logger::Debug);

    if (!args->undefinedOptions().isEmpty()) {
        Logger::error() << "backup failed: ";
        bool first = true;
        foreach (QString option, args->undefinedOptions()) {
            if (! first)
                Logger::error() << "          : ";
            Logger::error() << "unrecognized option or missing argument for; `" << option << "'" << Qt::endl;
            first = false;
        }
        return 1;
    }

    if (args->contains(QLatin1String("help"))) {
        args->usage(name(), argumentDescription());
        QString command = commandDescription();
        if (!command.isEmpty()) {
            Logger::standardOut() << Qt::endl;
            Logger::standardOut() << command;
        }
        return 0;
    }

    if (!args->parseErrors().isEmpty()) {
        Logger::error() << "backup failed:  ";
        foreach (QString e, args->parseErrors()) {
            Logger::error() << e << Qt::endl;
        }
        return 1;
    }

    QSettings appSettings;
    int size = appSettings.beginReadArray("repos");
    for (int i = 0; i < size; ++i) {
        appSettings.setArrayIndex(i);
        Repo repo;
        repo.location = appSettings.value("location").toString();
        repo.repoLocation = appSettings.value("repoLocation").toString();
        repo.lastSync = appSettings.value("lastSync").toString();
        repo.pwd = appSettings.value("pwd").toString();
        m_repos.append(repo);
    }
    appSettings.endArray();

    return run();
}

QString AbstractCommand::name() const
{
    return m_name;
}

void AbstractCommand::updateRepo(const AbstractCommand::Repo &repo)
{
    for (int i = 0; i < m_repos.size(); ++i) {
        if (repo.location == m_repos.at(i).location) {
            m_repos[i] = repo;
            break;
        }
    }
    saveRepos();
}

void AbstractCommand::saveRepos()
{
    QSettings appSettings;
    appSettings.beginWriteArray("repos");
    for (int i = 0; i < m_repos.size(); ++i) {
        const auto &r = m_repos[i];
        appSettings.setArrayIndex(i);
        appSettings.setValue("location", r.location);
        appSettings.setValue("repoLocation", r.repoLocation);
        appSettings.setValue("lastSync", r.lastSync);
        appSettings.setValue("pwd", r.pwd);
    }
    appSettings.endArray();
}
