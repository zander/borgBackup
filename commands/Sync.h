/*
 * This file is part of the borg-backup project
 * Copyright (C) 2008-2021 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef SYNC_H
#define SYNC_H

#include "../AbstractCommand.h"

class Sync : public AbstractCommand
{
public:
    Sync();

protected:
    QString argumentDescription() const override;
    int run() override;
    QString commandDescription() const override;

private:
    int doSync(const Repo &repo);
    void update(QStringList &list, const QString &item) const;


    struct Change {
        QString filename;
        QFileDevice::Permissions permissions;
        bool permissionsChanged = false;
        bool contentChanged = false;
    };

    Change findChange(const QString &line) const;
};

#endif
