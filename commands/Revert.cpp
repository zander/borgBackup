/*
 * This file is part of the borg-backup project
 * Copyright (C) 2008-2021 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Revert.h"
#include "../CommandLineParser.h"
#include "../Logger.h"
#include <BorgRunner.h>

Revert::Revert()
    : AbstractCommand("revert")
{
    CommandLineParser::setArgumentDefinition("revert");
}

int Revert::run()
{
    auto cur = QDir::currentPath();
    for (auto &repo : m_repos) {
        if (cur.startsWith(repo.location)) {
            return doRevert(repo);
        }
    }
    Logger::warn() << "Current dir is not a known backup dir." << Qt::endl;
    return 1;
}

int Revert::doRevert(const Repo &repo)
{
    QProcess borg;
    QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
    if (!repo.pwd.isEmpty())
        env.insert("BORG_PASSPHRASE", repo.pwd);
    env.insert("BORG_REPO", repo.repoLocation);
    borg.setProcessEnvironment(env);

    QDir workdir(repo.location);
    workdir.cdUp();
    QDir::setCurrent(workdir.absolutePath());

    BorgRunner runner(borg, QStringList() << "list" << "--format={mode} \"{path}\" -> \"{source}\"{LF}" << "::" + repo.lastSync);
    int err = runner.start(BorgRunner::WaitUntilFinished);
    if (err) return err;
    QStringList files;
    while (!borg.atEnd()) {
        QString line = QString::fromLocal8Bit(borg.readLine()).trimmed();
        int endQuote = line.indexOf("\"", 12);
        if (endQuote == -1)
            continue;
        QString path(line.mid(12, endQuote - 12));
        if (line.at(0) == 'd') { // dir
            if (!workdir.mkpath(path)) {
                Logger::error() << "Failed to create path: " << path << Qt::endl;
                return 2;
            }
        }
        else if (line.at(0) == 'h') { // hard link
            const int start = path.length() + 12 + 6;
            QString target(line.mid(start, line.length() - start - 1));
            QFile file(target);
            if (!file.exists()) {
                files << path;
                Logger::warn() << "Found missing: " << path << Qt::endl;
            }
        }
        else if (line.at(0) == 'l') { // sym link
            const int start = path.length() + 12 + 6;
            QString target(line.mid(start, line.length() - start - 1));
            const QString currentDest = QFile::symLinkTarget(path);
            if (currentDest.isEmpty()) {
                Logger::warn() << "File does not exist, making symlink: " << path << Qt::endl;
                Logger::warn() << " destination: " << target << Qt::endl;
                QFile destFile(target);
                bool ok = destFile.link(path);
                if (!ok) {
                    Logger::error() << "Failed to create symlink " + path << Qt::endl;
                    return 1;
                }
            }
        }
        else {
            QFile file(path);
            if (!file.exists()) {
                files << path;
                Logger::warn() << "Found missing: " << path << Qt::endl;

                if (files.size() > 100) {
                    err = fetchFiles(repo, files);
                    if (err) return err;
                    files.clear();
                }
            }
        }
    }

    if (!files.isEmpty())
        err = fetchFiles(repo, files);
    return err;
}

QString Revert::commandDescription() const
{
    return QLatin1String("Revert takes a repository and re-downloads all deleted files\n");
}

QString Revert::argumentDescription() const
{
    return QLatin1String("");
}

int Revert::fetchFiles(const AbstractCommand::Repo &repo, const QStringList &files)
{
    QProcess borg;
    QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
    if (!repo.pwd.isEmpty())
        env.insert("BORG_PASSPHRASE", repo.pwd);
    env.insert("BORG_REPO", repo.repoLocation);
    borg.setProcessEnvironment(env);

    Logger::info() << "Downloading " << files.size() << " files" << Qt::endl;
    QStringList args(files);
    args.insert(0, "::" + repo.lastSync);
    args.insert(0, "extract");
    BorgRunner runner(borg, args);
    return runner.start(BorgRunner::WaitUntilFinished);
}
