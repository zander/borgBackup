/*
 * This file is part of the borg-backup project
 * Copyright (C) 2008-2021 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Prune.h"

#include <BorgRunner.h>
#include <CommandLineParser.h>
#include <Logger.h>
#include <QDate>
#include <QProcess>
#include <QSet>

static const CommandLineOption options[] = {
     {"--all", "All"},
     {"--dry-run", "Dry-run, don't really change anything"},
    CommandLineLastOption
};


Prune::Prune()
    : AbstractCommand("sync")
{
    CommandLineParser::addOptionDefinitions(options);
    CommandLineParser::setArgumentDefinition("prune" );
}

QString Prune::argumentDescription() const
{
    return QLatin1String("");
}

QString Prune::commandDescription() const
{
    return QLatin1String("Sync syncs the current directory, or 'all' known dirs\n");
}

int Prune::run()
{
    auto args = CommandLineParser::instance();
    if (args->contains("all")) {
        for (auto repo : m_repos) {
            int ok = pruneRepo(repo);
            if (ok != 0)
                return ok;
        }
        return 0;
    }
    else {
        auto cur = QDir::currentPath();
        for (auto repo : m_repos) {
            if (cur.startsWith(repo.location)) {
                return pruneRepo(repo);
            }
        }
    }

    Logger::warn() << "Current dir is not a known backup dir." << Qt::endl;
    return 1;
}

struct Archive {
    Archive(const QList<QByteArray> &ids) {
        Q_ASSERT(ids.size() == 3);
        hostname = QString::fromUtf8(ids.at(0));
        name = ids.at(1);
        // date is in format: Sat, 2020-10-31 23:52:24
        const QByteArray rawDate = ids.at(2);
        int start = -1;
        int end = -1;
        for (int i = 0; i < rawDate.size(); ++i) {
            if (start == -1) {
                if (QChar(rawDate.at(i)).isDigit())
                    start = i;
            }
            else if (end == -1 && QChar(rawDate.at(i)).isSpace()) {
                end = i;
                break;
            }
        }
        if (start != -1 && end != -1) {
            QString s = QString::fromLatin1(rawDate.mid(start, end - start));
            date = QDate::fromString(s, "yyyy-MM-dd");
        }
        // else log warning
    }
    QString hostname;
    QByteArray name;
    QDate date;

    enum Keep {
        DontKeep,
        LastHost, // keep this because it is the most recent of this host
        LastOfDay // keep this because it is the last one of the day
    };
    Keep keep = DontKeep;
};

int Prune::pruneRepo(const AbstractCommand::Repo &repo)
{
    assert(!repo.repoLocation.isEmpty());
    assert(!repo.location.isEmpty());
    assert(!repo.lastSync.isEmpty());
    Logger::standardOut() << "Starting prune on " << repo.location << Qt::endl;

    QDir dir(repo.location);
    if (!dir.exists()) {
        Logger::warn() << "  WARN: location does not exist, skipping prune" << Qt::endl;
        return 0;
    }
    QDir workdir(dir);
    workdir.cdUp();

    QProcess borg;
    QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
    if (!repo.pwd.isEmpty())
        env.insert("BORG_PASSPHRASE", repo.pwd);
    env.insert("BORG_REPO", repo.repoLocation);
    borg.setProcessEnvironment(env);
    borg.setWorkingDirectory(workdir.absolutePath());

    BorgRunner runner(borg, QStringList() << "list" << "--format={hostname}{TAB}{archive}{TAB}{start}{LF}");
    int err = runner.start(BorgRunner::WaitUntilFinished);
    if (err) return err;
    auto output = borg.readAllStandardOutput();
    if (output.length() <= 36) {
        Logger::error() << "borg list failed, sorry\n";
        Logger::error() << borg.readAllStandardError() << Qt::endl;
        return 1;
    }

    // The rules of pruning:
    //   all archives are eligable for deletion.
    //   except for each host the last (most recent) one.
    //   We keep the last backup in a day.
    //   We keep the last 10 backups.
    //   we drop hosts that have not had a backup for more than 9 months.

    const int EXPIRE_HOST_MONTHS = 1;
    const int NUM_BACKUPS = 10;

    QList<Archive> archives;
    for (auto line : output.split('\n')) {
        auto chunks = line.split('\t');
        if (chunks.size() == 3)
            archives << Archive(chunks);
        else if (!line.isEmpty())
            Logger::error() << "Skipping unknown formatted line: " << line << Qt::endl;
    }
    // we loop from oldest backup to most recent one.
    QSet<QString> seenHosts;
    for (auto archive : archives) {
        if (!seenHosts.contains(archive.hostname)) {
            seenHosts.insert(archive.hostname);
            // Find the most recent archive for this host.
            for (int i = archives.size() - 1; i >= 0; --i) {
                if (archives.at(i).hostname == archive.hostname) {
                    if (archives.at(i).date.addMonths(EXPIRE_HOST_MONTHS) >= QDate::currentDate()) {
                        archives[i].keep = Archive::LastHost;
                        Logger::info() << "Marking archive 'keep' due to being the last of this host: "
                                   << QString::fromUtf8(archives.at(i).name) << Qt::endl;
                    }
                    break;
                }
            }
        }
    }

    QDate day;
    int kept = 0;
    for (int i = archives.size() - 1; i >= 0; --i) {
        if (day != archives.at(i).date) {
            day = archives.at(i).date;
            archives[i].keep = Archive::LastOfDay;
            Logger::info() << "Marking archive 'keep' due to being the last of the day: "
                       << QString::fromUtf8(archives.at(i).name) << Qt::endl;

            if (++kept >= NUM_BACKUPS)
                break;
        }
    }
    int amountToPrune = 0;
    for (auto archive : archives) {
        if (archive.keep == Archive::DontKeep)
            amountToPrune++;
    }
    Logger::standardOut() << "Pruning " << amountToPrune << " archives out of " << archives.size() << Qt::endl;

    const bool dryRun = CommandLineParser::instance()->contains("dry-run");
    int index = 1;
    for (auto archive : archives) {
        if (archive.keep == Archive::DontKeep) {
            Logger::standardOut() << index++ << "/" << amountToPrune << "] Pruning archive: (" << archive.date.toString()
                           << ") " << archive.name << " [" << archive.hostname << "]\n";
            Logger::standardOut().flush();

            if (!dryRun) {
                runner.setArguments(QStringList() << "delete" << QString("::%1").arg(QString::fromLatin1(archive.name)));
                err = runner.start(BorgRunner::WaitForStandardOutput);
                if (err) return err;
            }
        }
    }

    return 0; // Ok
}
