/*
 * This file is part of the borg-backup project
 * Copyright (C) 2008-2021 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Sync.h"
#include "../CommandLineParser.h"
#include "../Logger.h"
#include "../BorgRunner.h"

#include <QDateTime>

static const CommandLineOption options[] = {
     {"--all", "All"},
     {"--dry-run", "Dry-run, don't really change anything"},
    CommandLineLastOption
};

Sync::Sync()
    : AbstractCommand("sync")
{
    CommandLineParser::addOptionDefinitions(options);
    CommandLineParser::setArgumentDefinition("sync" );
}

int Sync::run()
{
    auto args = CommandLineParser::instance();
    if (args->contains("all")) {
        for (auto &repo : m_repos) {
            int ok = doSync(repo);
            if (ok != 0)
                return ok;
        }
        return 0;
    }
    else {
        auto cur = QDir::currentPath();
        for (auto &repo : m_repos) {
            if (cur.startsWith(repo.location)) {
                return doSync(repo);
            }
        }
    }

    Logger::warn() << "Current dir is not a known backup dir." << Qt::endl;
    return 1;
}

QString Sync::argumentDescription() const
{
    return QLatin1String("");
}

QString Sync::commandDescription() const
{
    return QLatin1String("Sync syncs the current directory, or 'all' known dirs\n");
}

int Sync::doSync(const AbstractCommand::Repo &repo)
{
    assert(!repo.repoLocation.isEmpty());
    assert(!repo.location.isEmpty());
    assert(!repo.lastSync.isEmpty());
    Logger::standardOut() << "Starting sync on " << repo.location << Qt::endl;

    QDir dir(repo.location);
    if (!dir.exists()) {
        Logger::warn() << "  WARN: location does not exist, skipping sync" << Qt::endl;
        return 0;
    }
    QDir workdir(dir);
    workdir.cdUp();

    QProcess borg;
    QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
    if (!repo.pwd.isEmpty())
        env.insert("BORG_PASSPHRASE", repo.pwd);
    env.insert("BORG_REPO", repo.repoLocation);
    borg.setProcessEnvironment(env);
    borg.setWorkingDirectory(workdir.absolutePath());

    BorgRunner runner(borg, QStringList() << "list" << "--last=1");
    int err = runner.start(BorgRunner::WaitUntilFinished);
    if (err) {
        Logger::error() << " Internal error, borg list returned with error code " << err;
        return err;
    }
    auto output = borg.readAllStandardOutput();
    if (output.length() <= 36) {
        Logger::error() << "borg list failed, sorry\n";
        Logger::error() << borg.readAllStandardError();
        return 1;
    }
    QString lastId = QString::fromUtf8(output.data(), 36);
    lastId = lastId.trimmed(); // remove leading/trailing spaces
    output.clear();

    const bool dryRun = CommandLineParser::instance()->contains("dry-run");
    if (lastId != repo.lastSync) {
        runner.setArguments(QStringList() << "diff" << "--same-chunker-params" << "::" + repo.lastSync << lastId);
        err = runner.start(BorgRunner::WaitUntilFinished);
        if (err) {
            Logger::error() << " Internal error; borg diff returned with error code " << err;
            return err;
        }

        QStringList added, removed;
        QVector<Change> changed;
        while (!borg.atEnd()) {
            QString line = QString::fromLocal8Bit(borg.readLine()).trimmed();
            if (line.startsWith("added "))
                update(added, line.mid(6));
            else if (line.startsWith("removed "))
                update(removed, line.mid(8));
            else
                changed.append(findChange(line));
        }

        for (auto &item : removed) {
            QFileInfo info(workdir.absolutePath() + '/' + item);
            Logger::info() << "removing " << item << (info.exists() ? "" : " [missing]") << Qt::endl;
            if (dryRun) continue;

            if (info.isDir()) {
                QDir itemIsDir(workdir.absolutePath() + '/' + item);
                bool ok = itemIsDir.removeRecursively();
                if (!ok)
                    Logger::warn() << "WARN: failed to remove dir: `" << item << "' from repo" << Qt::endl;
            }
            else if (info.exists()) {
                QFile file(workdir.absolutePath() + '/' + item);
                bool ok = file.remove();
                if (!ok)
                    Logger::warn() << "WARN: failed to remove file: `" << item << "' from repo" << Qt::endl;
            }
        }
        QStringList toDownload(added);
        for (auto &item : changed) {
            if (item.permissionsChanged && !item.contentChanged) {
                QFileInfo info(workdir.absolutePath() + '/' + item.filename);
                Logger::info() << "changing permissions of: `" << item.filename << "'" << (info.exists() ? "" : " [missing!]") << Qt::endl;
                if (dryRun) {
                    if (info.exists() && info.permissions() == item.permissions)
                        Logger::info() << "  [permissions unchanged]" << Qt::endl;
                } else {
                    QFile file (workdir.absolutePath() + '/' + item.filename);
                    bool ok = file.setPermissions(item.permissions);
                    if (!ok)
                        Logger::warn() << "WARN: failed to change permissions of file: `" << item.filename << "'" << Qt::endl;
                }
            }
            if (item.contentChanged)
                toDownload.append(item.filename);
        }
        Logger::info() << "downloading " << toDownload.size() << " files/dirs" << Qt::endl;

        if (toDownload.size() > 0) {
            toDownload.insert(0, "::" + lastId);
            toDownload.insert(0, "extract");
            runner.setArguments(toDownload);
            if (dryRun) {
                auto &log = Logger::info();
                log << "-- would call '";
                for (int i = std::min<int>(10, toDownload.size()); i > 0; --i) {
                    log << toDownload.takeFirst() << "'";
                    if (i > 1)
                        log << ", '";
                }
                if (!toDownload.isEmpty())
                    log << "... and " << toDownload.size() << " more" << Qt::endl;
                return 0;
            }
            err = runner.start(BorgRunner::WaitForStandardOutput);
            if (err) {
                Logger::error() << "Internal error, borg extract returned with error: " << err;
                return err;
            }
            do {
                auto data = borg.readAllStandardOutput();
                Logger::standardOut() << data;
                data = borg.readAllStandardError();
                Logger::standardOut() << data;
            } while (borg.state() != QProcess::NotRunning);
        }
    }
    if (dryRun) return 0;

    QFile hostnameFile("/etc/hostname");
    QString hostname("localhost");
    if (hostnameFile.open(QIODevice::ReadOnly)) {
        hostname = QString::fromUtf8(hostnameFile.readAll()).trimmed();
        hostnameFile.close();
    }

    QString projectName;
    for (auto &r : m_repos) {
        if (r.repoLocation == repo.repoLocation && r.location != repo.location) {
            // two locations sharing one repo on this machine.
            int index1 = repo.location.lastIndexOf('/');
            QString loc1 = index1 == -1 ? repo.location : repo.location.mid(index1 + 1);
            int index2 = r.location.lastIndexOf('/');
            QString loc2 = index2 == -1 ? r.location : r.location.mid(index2 + 1);
            if (loc1 != loc2) {
                projectName = loc1;
            }
            else {
                projectName = repo.location.left(10);
                projectName = projectName.remove('/');
            }
            break;
        }
    }

    const QString now = QDateTime::currentDateTime().toString("yyyyMMddhhmm");
    QString stamp = hostname;
    if (!projectName.isEmpty())
        stamp = stamp + '-' + projectName;
    stamp = stamp + '-' + now;
    if (dryRun) {
        Logger::info() << "Would create backup " << stamp << Qt::endl;
        return 0;
    }
    runner.setArguments(QStringList() << "create" << "-C" << "zstd" << "::" + stamp << dir.dirName());
    err = runner.start(BorgRunner::WaitForStandardOutput);
    if (err) {
        Logger::error() << " Internal error; borg create returned with error code: " << err;
        return err;
    }
    do {
        auto data = borg.readAllStandardOutput();
        Logger::standardOut() << data;
        data = borg.readAllStandardError();
        Logger::standardOut() << data;
    } while (borg.state() != QProcess::NotRunning);

    Repo repoCopy = repo;
    repoCopy.lastSync = stamp;
    updateRepo(repoCopy);
    return 0;
}

void Sync::update(QStringList &list, const QString &item) const
{
    int i = 0;
    bool afterDigits = false, afterBytes = false;
    bool isDirectory = item.startsWith("directory");
    if (isDirectory) {
        i = 9;
        afterDigits = true;
        afterBytes = true;
    }
    else if (item.startsWith("link")) {
        i = 4;
        afterDigits = true;
        afterBytes = true;
    }

    for (; i < item.size(); ++i) {
        QChar k = item.at(i);
        if (!afterBytes) {
            if ((k.unicode() == 'k' && item.size() > i && item.at(i + 1).unicode() == 'B')
                    || (k.unicode() == 'M' && item.size() > i && item.at(i + 1).unicode() == 'B')
                    || (k.unicode() == 'G' && item.size() > i && item.at(i + 1).unicode() == 'B')) {
                ++i;
                afterBytes = true;
                continue;
            }
            if (k.unicode() == 'B') {
                afterBytes= true;
                continue;
            }
        }
        if (k.unicode() == ' ' || (afterDigits && k.unicode() == '.'))
            continue;
        else if (k.isDigit())
            afterDigits = true;
        else
            break;
    }
    auto filename = item.mid(i);
    for (const auto &item : list) {
        if (filename.startsWith(item))
            return;
    }
    list.append(filename);
}

Sync::Change Sync::findChange(const QString &line) const
{
    // format:
    // +230 B    -251 B cutestuff/INSTALL
    // +20 B      -6 B README

    // format:
    // [-rwxrwxr-x -> -rwx------] projects/git/blog.git/hooks/pre-commit.sample
    // +1.2 kB   -1.5 kB [-rwxrwxr-x -> -rwx------] projects/git/blog.git/hooks/prepare-commit-msg.sample
    // +4.9 kB   -4.9 kB [-rwxrwxr-x -> -rwx------] projects/git/blog.git/hooks/pre-rebase.sample

    // Format on changing user/group
    // [garfield:users -> garfield:garfield] Folder
    //    0 B       0 B [garfield:users -> garfield:garfield] projects/bla

    Change change;
    int i = 0;
    bool afterMinus = false, afterDigits = false, metaBlockEnded = false;
    for (; i < line.size(); ++i) {
        QChar k = line.at(i);
        if (k.isSpace())
            continue;
        if (k.unicode() == '[') {
            const int colonPos = line.indexOf(':', i);
            const int closeBrace = line.indexOf(']', i);
            if (colonPos != -1 && colonPos < closeBrace) {
                // group or user change.
                // We don't support that, so we don't record the change.
                metaBlockEnded = true;
                i = closeBrace;
                continue;
            }
            else if (line.size() > i + 26 && line.at(i + 25).unicode() == ']') {
                // mode change.
                change.permissionsChanged = true;
                const QChar *data = line.data() + i + 16;
                if (data[0] == 'r') change.permissions |= QFileDevice::ReadOwner;
                if (data[1] == 'w') change.permissions |= QFileDevice::WriteOwner;
                if (data[2] == 'x') change.permissions |= QFileDevice::ExeOwner;
                if (data[3] == 'r') change.permissions |= QFileDevice::ReadGroup;
                if (data[5] == 'w') change.permissions |= QFileDevice::WriteGroup;
                if (data[6] == 'x') change.permissions |= QFileDevice::ExeGroup;
                if (data[7] == 'r') change.permissions |= QFileDevice::ReadOther;
                if (data[8] == 'w') change.permissions |= QFileDevice::WriteOther;
                if (data[9] == 'x') change.permissions |= QFileDevice::ExeOther;

                i += 25;
                metaBlockEnded = true;
                continue;
            }
        }
        if (metaBlockEnded)
            break;
        if (!afterMinus && k.unicode() != '-')
            continue;
        if (k.unicode() == '-') {
            afterMinus = true;
            change.contentChanged = true;
        }
        else if (afterMinus && (k.isDigit() || k.unicode() == '.')) {
            afterDigits = true;
        }
        else if (afterDigits && k.unicode() == 'B') {
            change.contentChanged = true;
            metaBlockEnded = true;
        }
    }
    change.filename = line.mid(i);
    return change;
}
