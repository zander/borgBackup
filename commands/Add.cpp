/*
 * This file is part of the borg-backup project
 * Copyright (C) 2008-2021 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Add.h"
#include "../CommandLineParser.h"
#include "../Logger.h"

#include <BorgRunner.h>

static const CommandLineOption options[] = {
     {"--pwd PWD", "Remember the password"},
     {"-f, --force", "Update existing entry"},
     {"-s, --state STATE", "Set which commit this repo is at"},
    CommandLineLastOption
};

Add::Add()
    : AbstractCommand("add")
{
    CommandLineParser::addOptionDefinitions(options);
    CommandLineParser::setArgumentDefinition("add <DIRECTORY> <REPO>");
}

int Add::run()
{
    auto args = CommandLineParser::instance();
    Logger::info() << "trying " << args->arguments().at(1) << " / " << args->arguments().at(2) << Qt::endl;
    QDir dir(args->arguments().at(1));
    if (!dir.exists()) {
        Logger::error() << "Argument is not an existing directory: " << args->arguments().at(1) << Qt::endl;
        return 1;
    }
    QString location = dir.absolutePath();
    int i = 0;
    for (; i < m_repos.length(); ++i) {
        if (m_repos.at(i).location == location) {
            if (!args->contains("force")) {
                Logger::error() << "Added dir already known." << Qt::endl;
                return 1;
            }
            break;
        }
    }
    Repo repo;
    if (m_repos.length() > i)
        repo = m_repos.at(i);
    repo.location = location;
    repo.repoLocation = args->arguments().at(2);
    if (args->contains("pwd"))
        repo.pwd = args->optionArgument("pwd");
    if (args->contains("state"))
        repo.lastSync = args->optionArgument("state");

    if (repo.lastSync.isEmpty()) {
        QProcess borg;
        QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
        if (!repo.pwd.isEmpty())
            env.insert("BORG_PASSPHRASE", repo.pwd);
        env.insert("BORG_REPO", repo.repoLocation);
        borg.setProcessEnvironment(env);
        borg.setWorkingDirectory(repo.location);

        BorgRunner runner(borg, QStringList() << "list");
        int err = runner.start(BorgRunner::WaitUntilFinished);
        if (err) return err;
        auto output = borg.readAllStandardOutput();

        Logger::standardOut() << "Repo requires a 'state', please supply one from the following list\n\n";
        Logger::standardOut() << QString::fromLocal8Bit(output);
        return 0;
    }

    if (m_repos.size() <= i)
        m_repos.append(repo);
    else
        m_repos[i] = repo;
    saveRepos();
    return 0;
}

QString Add::argumentDescription() const
{
    return QLatin1String("<DIRECTORY> <REPO>");
}

QString Add::commandDescription() const
{
    return QLatin1String("Add remembers a location as one that contains a borg backup\n"
                         "The remote backup location is the REPO and the DIRECTORY should\n"
                         "be a local, existing directory.");
}
