/*
 * This file is part of the borg-backup project
 * Copyright (C) 2008-2021 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "List.h"
#include "../Logger.h"
#include "../CommandLineParser.h"

List::List()
    : AbstractCommand("list")
{
    CommandLineParser::setArgumentDefinition("list" );
}

int List::run()
{
    for (auto repo : m_repos) {
        Logger::standardOut() << repo.location << Qt::endl;
        Logger::info() << " +-> " << repo.repoLocation << Qt::endl;
    }
    return 0;
}

QString List::argumentDescription() const
{
    return QLatin1String();
}

QString List::commandDescription() const
{
    return QLatin1String("List simply lists all directories and repos known to backup.\n");
}
