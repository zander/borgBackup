/*
 * This file is part of the borg-backup project
 * Copyright (C) 2008-2021 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ABSTRACTCOMMAND_H
#define ABSTRACTCOMMAND_H

#include <QDir>
#include <QString>

/**
 * This baseclass can be used to inherit from in order to add new top-level commands to backup.
 * Top level commands are things like 'whatsnew' and 'add'.
 * @see CommandLineParser
 */
class AbstractCommand {
public:
    /**
     * Constructor.
     */
    AbstractCommand(const char *name);
    virtual ~AbstractCommand();

    int start();

    /// return the name of this command.
    QString name() const;

protected:
    struct Repo {
        QString location;
        QString repoLocation;
        QString lastSync;
        QString pwd;
    };
    QList<Repo> m_repos;


    /**
     * return the short visual description of the arguments you can pass to this command.
     * If the command takes arguments that are not options, you can make the help message print that
     * by returning something in this method.
     */
    virtual QString argumentDescription() const = 0;

    /**
     * return long (multiline) Description of the command.
     */
    virtual QString commandDescription() const = 0;

    /**
     * Run the commmand from the repository root.
     * @return the exit code as used by the comamnd.
     * @see ReturnCodes
     */
    virtual int run() = 0;

    void updateRepo(const Repo &repo);
    void saveRepos();

private:
    QString m_name;
};

#endif
