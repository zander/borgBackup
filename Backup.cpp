/*
 * This file is part of the borg-backup project
 * Copyright (C) 2008-2021 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "AbstractCommand.h"
#include "Backup.h"
#include "CommandLineParser.h"
#include "Logger.h"

// commands
#include "commands/Add.h"
#include "commands/List.h"
#include "commands/Sync.h"
#include "commands/Revert.h"
#include "commands/Prune.h"

#include <QTextStream>

#define VERSION_NUM "0.6.2"
#define VERSION "backup version " VERSION_NUM

BackupCommandLine::BackupCommandLine(int argc, char **argv)
    : QCoreApplication(argc, argv)
{
    setOrganizationName("zander");
    setApplicationName("backup");
    setApplicationVersion(VERSION_NUM);

    if (argc > 1)
        m_command = QString::fromLocal8Bit(argv[1]);
    if (m_command == QLatin1String("help") && argc > 2) { // 'help foo' -> 'foo -h'
        argv[1] = argv[2];
        argv[2] = new char[3];
        argv[2][0] = '-';
        argv[2][1] = 'h';
        argv[2][2] = 0;
        m_command = QString::fromLocal8Bit(argv[1]);
    }

    CommandLineParser::init(argc, argv);
}

BackupCommandLine::~BackupCommandLine()
{
    delete CommandLineParser::instance();
}

int BackupCommandLine::run()
{
    if (m_command == QLatin1String("--version") || m_command == QLatin1String("-v")) {
        QTextStream out(stdout);
        out << VERSION << Qt::endl;
        return 0;
    }
    if (m_command.isEmpty()) {
        printHelp();
        return 0;
    }

    // autoComplete command
    QStringList commands;
    // keep this list sorted in the sourcecode.    In vim just select the lines and do a :'<,'>!sort
    commands << QLatin1String("add");
    commands << QLatin1String("list");
    commands << QLatin1String("prune");
    commands << QLatin1String("revert");
    commands << QLatin1String("sync");

    QString command;
    foreach (QString s, commands) {
        if (m_command == s) { // perfect match
            command = m_command;
            break;
        }
        if (! command.isEmpty()) {
            if (s.startsWith(m_command)) {
                QTextStream out(stdout);
                out << "backup failed: " << "Ambiguous command..." << Qt::endl;
                return -1;
            } else {
                break; // found best match.
            }
        }
        if (s.startsWith(m_command)) {
            command = s;
            continue;
        }
    }
    if (command.isEmpty()) {
        if (! m_command.isEmpty()) {
            QTextStream out(stdout);
            if (m_command.startsWith('-'))
                out << "backup failed: " << "Missing command, please choose one\n";
            else
                out << "backup failed: " << "Invalid command `" << m_command << "'\n";
        }
        printHelp();
        return 0;
    }
    m_command = command;

    AbstractCommand *ac = 0;
    if (m_command == QLatin1String("add"))
        ac = new Add();
    else if (m_command == QLatin1String("list"))
        ac = new List();
    else if (m_command == QLatin1String("sync"))
        ac = new Sync();
    else if (m_command == QLatin1String("prune"))
        ac = new Prune();
    else if (m_command == QLatin1String("revert"))
        ac = new Revert();
    else if (command == QLatin1String("help")) {
        printHelp();
        return 0;
    }

    Q_ASSERT(ac);
    return ac->start();
}

void BackupCommandLine::printHelp()
{
    QTextStream out(stdout);
    out << "Usage: backup COMMAND ...\n\n" << VERSION << Qt::endl;
    out << "Commands:" << Qt::endl;
    out << "  help          Display help for backup or a single commands." << Qt::endl;
    out << "Changing and querying the backups we monitor:" << Qt::endl;
    out << "  add           Add a new directory-structure for backup." << Qt::endl;
    out << "  list          List all the known backup locations." << Qt::endl;
    out << "Backups:" << Qt::endl;
    out << "  sync          Actually sync a copy to the latest version" << Qt::endl;
    out << "  prune         Prune historical backups" << Qt::endl;
    out << "  revert        Download all missing files from the backup" << Qt::endl;
    out << Qt::endl;
    out << "Use 'backup COMMAND --help' for help on a single command." << Qt::endl;
    out << "Use 'backup --version' to see the backup version number." << Qt::endl;
}


// a C main function
int main(int argc, char **argv)
{
    BackupCommandLine backup(argc, argv);
    return backup.run();
}
