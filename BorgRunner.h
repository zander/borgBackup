/*
 * This file is part of the borg-backup project
 * Copyright (C) 2008-2021 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef BORGRUNNER_H
#define BORGRUNNER_H

#include <QProcess>
#include <QStringList>

/**
 * Class to easilly run borg in a sub process.
 */
class BorgRunner
{
public:
    enum WaitCondition {
        WaitForStandardOutput,
        WaitForStandardError,
        WaitUntilFinished,
        WaitUntilReadyForWrite
    };
    enum Fail {
        FailureAccepted, // if borg returns with non-zero return code, thats ok.
        WarnOnFail      // Log an error if borg returns with non-zero return code.
    };
    BorgRunner(QProcess &process, const QStringList &arguments);
    int start(WaitCondition condition, Fail fail = WarnOnFail);

    void setArguments(const QStringList &arguments);

    void setTimeout(int msecs) { m_timeout = msecs; }
    int timeout() const { return m_timeout; }

private:
    QProcess *m_process;
    QStringList m_arguments;
    int m_timeout;
};

#endif
